# Figures

This is a place that saves my figures.

The figures are drew for my personal use.

The platform of making figures is [https://app.diagrams.net/](https://app.diagrams.net/).

It can automatically save the original files.

Enjoy drawing!
